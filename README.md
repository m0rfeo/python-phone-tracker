# Python Phone Tracker

This simply program gives some useful info about phone number (Country, Company and Timezone).

## Requirement

- Install Python Module PhoneNumbers.
```
pip install phonenumbers 
```

- Give execution permissions to the program.
```
sudo chmod +x phone_info.py
```

## Example of Usage

We will simulate to track: +34123456789
```
❯ ./phone_info.py
Number to analyze: +34123456789
-----------------
- Country Code: 34 National Number: 123456789
- Company: Jazztle
- Country: Spain
- Timezone: Europe/Madrid
```

## License

Like all my projects this program is licensed under GPL 3.0 License. You can do anything that you want with my work except change license type, I will be graeful if you mention me.

## Misc

Author: m0rfeo

Open to suggestions :)
