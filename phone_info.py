#!/usr/bin/env python
# Provided by m0rfeo, GPL 3.0 License
# Module used
import phonenumbers
from phonenumbers import geocoder   # Location of the phone number
from phonenumbers import carrier    # Provider of the phone number
from phonenumbers import timezone   # Timezone of the phone number

phone=(input("Number to analyze: "))
print("-----------------")

# Separate Country Code and Number on the phone number provided to analyze
target_number = phonenumbers.parse(phone)

# Info about phone number
validation = phonenumbers.is_valid_number(target_number)            # Bool, Check if the number provided exist
country = geocoder.country_name_for_number(target_number, "en")     # Str,  Country of the Phone
company = carrier.name_for_number(target_number, "en")              # Str,  Company of the Phone
tz = timezone.time_zones_for_geographical_number(target_number)     # Str,  Timezone Tuple
tz_clean = str = ''.join(tz)                                        # Convert Tuple to String, for beautiful output 

# Print info about phone number if exists
if validation == True:
    print("-",target_number)
    print("- Company:",company)
    print("- Country:",country)
    print("- Timezone:",tz_clean)
else:
    print("The phone",phone,"doesn't exist")



